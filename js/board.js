import Tile from "./tile.js"

export default class Board {
    constructor(rows, columns, tileWidth) {
        this.rows = rows;
        this.columns = columns;
        this.tileWidth = tileWidth;
        this.tiles = [];
        this.emptyTileIndex = null;
        // this.element = document.getElementById('board');
        this.element = document.createElement('div');
        this.element.id = 'board';
        this.element.style.gridTemplateColumns = `repeat(${columns}, ${tileWidth}px)`;
        this.element.style.width = `${columns * tileWidth}px`;
        this.element.style.height = `${rows * tileWidth}px`;
        this.element.addEventListener('click', () => {
            // add logic for clicking the board here
        });
        document.body.appendChild(this.element);

        this.orders = Array.from({ length: this.rows * this.columns - 1 }, (_, i) => i + 1);

        this.shuffle()

        this.isTransitioning = false
    }

    onTileMouseDown(index) {
        // Save the mouse down position and the tile index
        this.mouseDownPosition = { x: event.clientX, y: event.clientY };
        this.draggedTileIndex = index;
    }

    onTileMouseMove(index) {
        // If a tile is being dragged, move it to the current mouse position
        if (this.draggedTileIndex !== undefined) {
            const dx = event.clientX - this.mouseDownPosition.x;
            const dy = event.clientY - this.mouseDownPosition.y;
            this.moveTile(dx, dy, this.determineNearEmpty(this.draggedTileIndex))
        }
    }

    moveTile(dx, dy, position) {
        const tileElement = this.element.children[this.draggedTileIndex]

        const tileRect = tileElement.getBoundingClientRect();

        this.emptyElement = this.element.children[this.emptyIndex]

        const emptyRect = this.emptyElement.getBoundingClientRect();

        switch (position) {
            case 'before':
                if (dx < 0 && !this.isTransitioning) {
                    tileElement.style.transform = `translateX(${emptyRect.x - tileRect.x}px`;
                    this.emptyElement.style.transform = `translateX(${tileRect.x - emptyRect.x}px`;
                    this.draggedTileIndex = undefined;
                }
                break;
            case 'after':
                if (dx > 0 && !this.isTransitioning) {
                    tileElement.style.transform = `translateX(${emptyRect.x - tileRect.x}px`
                    this.emptyElement.style.transform = `translateX(${tileRect.x - emptyRect.x}px`;
                    this.draggedTileIndex = undefined;
                }
                break;
            case 'up':
                if (dy < 0 && !this.isTransitioning) {
                    tileElement.style.transform = `translateY(${emptyRect.y - tileRect.y}px`
                    this.emptyElement.style.transform = `translateY(${tileRect.y - emptyRect.y}px`;
                    this.draggedTileIndex = undefined;
                }
                break;
            case 'down':
                if (dy > 0 && !this.isTransitioning) {
                    tileElement.style.transform = `translateY(${emptyRect.y - tileRect.y}px`
                    this.emptyElement.style.transform = `translateY(${tileRect.y - emptyRect.y}px`;
                    this.draggedTileIndex = undefined;
                }
                break;
            default:
                return true
        }

    }

    onTileMouseUp(index) {
        if (this.draggedTileIndex !== undefined) {
            this.mouseDownPosition = undefined;
            this.draggedTileIndex = undefined;
        }
    }

    swapTile(fromIndex, toIndex) {


        const temp = this.orders[fromIndex];
        this.orders[fromIndex] = null;
        this.orders[toIndex] = temp;

        const boardElement = this.element;

        const tile = new Tile(this.orders[toIndex]);
        tile.element.style.width = `${this.tileWidth}px`;
        tile.element.style.height = `${this.tileWidth}px`;

        boardElement.replaceChild(tile.element, boardElement.children[toIndex])


        const emptyTile = new Tile(this.orders[fromIndex]);
        emptyTile.element.style.width = `${this.tileWidth}px`;
        emptyTile.element.style.height = `${this.tileWidth}px`;

        emptyTile.element.classList.add('empty');

        boardElement.replaceChild(emptyTile.element, boardElement.children[fromIndex])

        this.addListener()

        if (this.isWon()) {
            alert('Congratulations you won')
        }

    }


    determineNearEmpty(index) {
        this.emptyIndex = this.orders.findIndex(item => !item)

        if (this.emptyIndex + 1 === index && (this.emptyIndex + 1) % this.rows !== 0) return 'before'
        if (this.emptyIndex - 1 === index && (index + 1) % this.rows !== 0) return 'after'
        if (this.emptyIndex + this.rows === index) return 'up'
        if (this.emptyIndex - this.rows === index) return 'down'

    }

    // shuffle the tiles randomly
    shuffle() {
        this.orders.push(null);
        for (let i = this.orders.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [this.orders[i], this.orders[j]] = [this.orders[j], this.orders[i]];
        }

        this.tiles = this.orders.map((number, i) => {
            const tile = new Tile(number);
            tile.element.style.width = `${this.tileWidth}px`;
            tile.element.style.height = `${this.tileWidth}px`;
            this.element.appendChild(tile.element);
            if (number === null) {
                this.emptyTileIndex = i;
                tile.element.classList.add('empty');
            }
            return tile;
        });
    }

    onTransitionEnd(index) {
        if (this.emptyIndex !== index && this.orders[index]) {
            this.swapTile(index, this.emptyIndex);
            this.isTransitioning = false
        }

    }
    onTransitionStart(index) {
        this.isTransitioning = true
    }

    addListener() {
        const boardElement = this.element;

        const tileElements = boardElement.querySelectorAll('.tile');

        tileElements.forEach((tile, index) => {
            tile.addEventListener('mousedown', () => this.onTileMouseDown(index));
            tile.addEventListener('mousemove', () => this.onTileMouseMove(index));
            tile.addEventListener('mouseup', () => this.onTileMouseUp(index));
            tile.addEventListener('transitionend', () => this.onTransitionEnd(index));
            tile.addEventListener('transitionstart', () => this.onTransitionStart(index));
        });
    }

    isWon() {
        for (let i = 0; i < this.orders.length - 1; i++) {
            if (this.orders[i] !== i + 1) {
                return false;
            }
        }
        return this.orders[this.orders.length - 1] === null || this.orders[this.orders.length - 1] === this.tiles.length;
    }
}



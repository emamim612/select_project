// define a Tile class to represent each puzzle tile
export default class Tile {
    constructor(number) {
      this.number = number;
      this.element = document.createElement('div');
      this.element.classList.add('tile');
      this.element.textContent = number;
      this.element.addEventListener('click', () => {
        // add logic for clicking a tile here
      });
    }
  }
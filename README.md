# SELECT TEST PROJECT

A simple web app to solve the classic 8-tiles puzzle game.

## Installation

Before running the app, make sure you have Node.js installed on your machine. Then, install the `serve` package globally by running:

`npm install -g serve`

## Usage

To run the app, navigate to the project directory and run:

`serve`